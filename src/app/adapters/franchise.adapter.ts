import { InjectionToken } from "@angular/core";
import { Franchise } from "../entities/franchise";
import { Adapter } from "./adapter";

export const FranchiseAdapterInjectionToken
    = new InjectionToken<Adapter<Franchise>>('Adapter');

export class FranchiseAdapter implements Adapter<Franchise> {
    adapt(item: any): Franchise {
        const franchise = new Franchise();

        if (item) {
            franchise.label = item.l;
        }
        
        return franchise;
    }

}