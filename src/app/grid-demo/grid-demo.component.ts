import { Component, OnInit, ViewChild } from '@angular/core';
import { GridComponent } from '@core/components/grid/grid.component';

@Component({
  selector: 'app-grid-demo',
  templateUrl: './grid-demo.component.html',
  styleUrls: ['./grid-demo.component.scss']
})
export class GridDemoComponent implements OnInit {

  @ViewChild(GridComponent, { static: true }) public grid?: GridComponent;

  constructor() { }

  ngOnInit(): void {

    //  this.grid?.helloFromGrid();
  }

  public clickMe() {
    alert('clicked!');
  }
}
