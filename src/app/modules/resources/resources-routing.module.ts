import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateUpdateResourceReactiveComponent } from './components/create-update-resource-reactive/create-update-resource-reactive.component';
import { CreateUpdateResourceComponent } from './components/create-update-resource/create-update-resource.component';
import { ResourceListComponent } from './components/resource-list/resource-list.component';

const routes: Routes = [

  // { path: 'create', component: CreateUpdateResourceComponent },
  // { path: 'update/:id', component: CreateUpdateResourceComponent },
  { path: 'create', component: CreateUpdateResourceReactiveComponent },
  { path: 'update/:id', component: CreateUpdateResourceReactiveComponent },
  { path: '**', component: ResourceListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResourcesRoutingModule { }
