import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResourcesRoutingModule } from './resources-routing.module';
import { ResourceListComponent } from './components/resource-list/resource-list.component';
import { MaterialModule } from '../material/material.module';
import { CreateUpdateResourceComponent } from './components/create-update-resource/create-update-resource.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';
import { CreateUpdateResourceReactiveComponent } from './components/create-update-resource-reactive/create-update-resource-reactive.component';


@NgModule({
  declarations: [
    ResourceListComponent,
    CreateUpdateResourceComponent,
    CreateUpdateResourceReactiveComponent
  ],
  imports: [
    ResourcesRoutingModule,
    MaterialModule,
    CommonModule,
    FormsModule,
    CoreModule,
    ReactiveFormsModule
  ]
})
export class ResourcesModule { }
