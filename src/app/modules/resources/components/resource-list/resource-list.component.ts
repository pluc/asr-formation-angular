import { AfterViewInit, ChangeDetectorRef, Component, OnInit, QueryList, TemplateRef, ViewChildren } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/app.service';
import { Resource } from 'src/app/entities/resource';
import { ResourceService } from '../../services/resource.service';

@UntilDestroy()
@Component({
  selector: 'app-resource-list',
  templateUrl: './resource-list.component.html',
  styleUrls: ['./resource-list.component.scss']
})
export class ResourceListComponent implements OnInit {

  public resourcesList$?: Observable<Resource[]>;

  constructor(
    private resourceService: ResourceService,
    private appService: AppService) {
    this.appService.notifyPageTitle('Liste des ressources');
  }


  ngOnInit(): void {

  

    this.resourcesList$ = this.resourceService.getAllResources();
  }

  public deleteResource(resource: Resource): void {
    this.resourceService.deleteResource(resource)
      .pipe(untilDestroyed(this))
      .subscribe(r => {
        if (!r) {
          alert('Erreur de suppression');
        }
      });
  }
}
