import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { AppService } from 'src/app/app.service';
import { Resource } from 'src/app/entities/resource';
import { uniqueValidator } from 'src/app/modules/core/validators/unique.validator';
import { ResourceService } from '../../services/resource.service';

@UntilDestroy()
@Component({
  selector: 'app-create-update-resource-reactive',
  templateUrl: './create-update-resource-reactive.component.html',
  styleUrls: ['./create-update-resource-reactive.component.scss']
})
export class CreateUpdateResourceReactiveComponent implements OnInit {

  public formGroup?: FormGroup;

  private resource?: Resource;


  public isInCreation: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private resourceService: ResourceService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private appService: AppService) {

    this.isInCreation = location.pathname == '/resources/create';
    this.appService.notifyPageTitle(this.isInCreation ? 'Création d\'une ressource' : 'Modification d\'une ressource');
  }

  ngOnInit(): void {
    const routeId = this.activatedRoute.snapshot.paramMap.get('id');

    this.resourceService.getResource(Number(routeId))
      .pipe(untilDestroyed(this))
      .subscribe((r: Resource | undefined) => {
        if (!r) {
          r = new Resource('', '');
        }
        this.resource = r;

        this.isInCreation = this.resource.id == null;

        this.formGroup = this.formBuilder.group({
          code: [this.resource.code,
          [
            Validators.required,
            Validators.minLength(3),
          ], [
            uniqueValidator(this.resourceService.getAllCodesResources())]],
          label: [this.resource.label, [Validators.required, Validators.minLength(5)]]
        });

        // this.formGroup.get('code')?.setValue('toto');
      });

  }

  public test() {
    console.info(this.formGroup?.get('code'))
  }

  public validateForm(): void {
    if (this.resource == null) {
      return;
    }

    Object.assign(this.resource, this.formGroup?.value);

    if (this.isInCreation) {
      this.resourceService.addResource(this.resource)
        .pipe(untilDestroyed(this))
        .subscribe(s => {
          if (s) {
            this.router.navigate(['/resources']);
          } else {
            alert('Erreur lors de lajout');
          }
        });
    } else {
      this.resourceService.updateResource(this.resource)
        .pipe(untilDestroyed(this))
        .subscribe(s => {
          if (s) {
            this.router.navigate(['/resources']);
          } else {
            alert('Erreur lors de la modification');
          }
        })
    }
  }

  public cancel(): void {
    this.router.navigate(['/resources']);
  }

}
