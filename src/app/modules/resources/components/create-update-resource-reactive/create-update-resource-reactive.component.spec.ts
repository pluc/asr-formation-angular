import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUpdateResourceReactiveComponent } from './create-update-resource-reactive.component';

describe('CreateUpdateResourceReactiveComponent', () => {
  let component: CreateUpdateResourceReactiveComponent;
  let fixture: ComponentFixture<CreateUpdateResourceReactiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateUpdateResourceReactiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUpdateResourceReactiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
