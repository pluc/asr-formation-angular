import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ResourceService } from '../../services/resource.service';
import { CreateUpdateResourceComponent } from './create-update-resource.component';


describe('CreateUpdateResourceComponent', () => {
  let component: CreateUpdateResourceComponent;
  let fixture: ComponentFixture<CreateUpdateResourceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreateUpdateResourceComponent],
      imports: [RouterTestingModule.withRoutes([])],
      providers: [
        ResourceService
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUpdateResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
