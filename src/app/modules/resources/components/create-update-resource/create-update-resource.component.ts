import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { AppService } from 'src/app/app.service';
import { Resource } from 'src/app/entities/resource';
import { ResourceService } from '../../services/resource.service';

@UntilDestroy()
@Component({
  selector: 'app-create-update-resource',
  templateUrl: './create-update-resource.component.html',
  styleUrls: ['./create-update-resource.component.scss']
})
export class CreateUpdateResourceComponent implements OnInit {

  private resource?: Resource;
  public copy?: Resource;
  public isInCreation: boolean = false;

  constructor(
    private resourceService: ResourceService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private appService: AppService) {

    this.isInCreation = location.pathname == '/resources/create';
    this.appService.notifyPageTitle(this.isInCreation ? 'Création d\'une ressource' : 'Modification d\'une ressource');
  }

  ngOnInit(): void {
    const routeId = this.activatedRoute.snapshot.paramMap.get('id');

    this.resourceService.getResource(Number(routeId))
      .pipe(untilDestroyed(this))
      .subscribe((r: Resource | undefined) => {
        if (!r) {
          r = new Resource('', '');
        }
        this.resource = r;

        this.copy = new Resource('', '');
        Object.assign(this.copy, this.resource);

        this.isInCreation = this.resource.id == null;
      });
  }

  public validateForm(): void {
    if (this.resource == null) {
      return;
    }

    Object.assign(this.resource, this.copy);

    if (this.isInCreation) {
      this.resourceService.addResource(this.resource)
        .pipe(untilDestroyed(this))
        .subscribe(s => {
          if (s) {
            this.router.navigate(['/resources']);
          } else {
            alert('Erreur lors de lajout');
          }
        });
    } else {
      this.resourceService.updateResource(this.resource)
        .pipe(untilDestroyed(this))
        .subscribe(s => {
          if (s) {
            this.router.navigate(['/resources']);
          } else {
            alert('Erreur lors de la modification');
          }
        })
    }
  }

  public cancel(): void {
    this.router.navigate(['/resources']);
  }
}
