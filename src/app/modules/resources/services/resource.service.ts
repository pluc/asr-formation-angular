import { Injectable } from '@angular/core';
import { Resource } from '@entities';
import * as _ from 'lodash';
import { isNumber } from 'lodash';
import { BehaviorSubject, catchError, map, Observable, of } from 'rxjs';

let resourceIdCpt = 0;

@Injectable({
  providedIn: 'root'
})
export class ResourceService {
  private _allResources: Resource[] = [
    new Resource('Resource 1', 'RES_1', 1),
    new Resource('Resource 2', 'RES_2', 2)
  ];

  private _allResources$: BehaviorSubject<Resource[]>
    = new BehaviorSubject<Resource[]>([...this._allResources]);

  constructor() {
  }

  public notifyRessources(): void {
    this._allResources$.next([...this._allResources]);
  }

  public addResource(resource: Resource): Observable<boolean> {
    return of(null).pipe(
      map(() => {
        if (resource.id == null) {
          // Stockage d'un id négatif pour que le backend sache que la ressource n'existe pas encore
          resource.id = --resourceIdCpt;
        } else {
          throw "Id not null";
        }

        this._allResources.push(resource);

        this.notifyRessources();
        return true;
      }),
      catchError(e => {
        console.error(e);
        return of(false);
      })
    );
  }

  public updateResource(resource: Resource): Observable<boolean> {
    return of(null).pipe(
      map(() => {
        this.notifyRessources();
        return true;
      }),
      catchError(e => {
        console.error(e);
        return of(false);
      })
    );
  }

  public getResource(id: number): Observable<Resource | undefined> {
    return of(null).pipe(
      map(() => this._allResources.find(r => r.id == id)),
      catchError(e => {
        console.error(e);
        return of(undefined);
      })
    );
  }

  public deleteResource(resource: Resource | number): Observable<boolean> {

    return of(null).pipe(
      map(() => {
        const isNumberParam: boolean = isNumber(resource);

        if (isNumberParam) {
          _.remove(this._allResources, r => r.id == resource);
        } else {
          _.remove(this._allResources, r => r == resource);
        }

        this.notifyRessources();

        return true;
      }),
      catchError(e => {
        console.error(e);
        return of(false);
      })
    );
  }

  public getAllCodesResources(): Observable<string[]> {
    return this.getAllResources().pipe(
      map((resources: Resource[]) => {
        const codes: string[] = resources.map(r => r.code);

        return codes;
      }),
      catchError(e => {
        console.error(e);
        return of([]);
      })
    );
  }

  public getAllResources(): Observable<Resource[]> {
    return this._allResources$.asObservable().pipe(
      map(r => {
        console.log('resourrces ready');
        return r;
      }),
      catchError(e => {
        console.error(e);
        return of([]);
      })
    );
  }

}
