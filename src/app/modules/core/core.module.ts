import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { ConfirmDirective } from './directives/confirm.directive';
import { GridComponent } from './components/grid/grid.component';
import { ContentTemplateDirective, FooterTemplateDirective, HeaderTemplateDirective } from './directives/grid.directives';
import { TakePictureComponent } from './components/take-picture/take-picture.component';



@NgModule({
  declarations: [
    ConfirmComponent,
    ConfirmDirective,
    GridComponent,
    HeaderTemplateDirective,
    ContentTemplateDirective,
    FooterTemplateDirective,
    TakePictureComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    ConfirmDirective, 
    GridComponent,
    HeaderTemplateDirective,
    ContentTemplateDirective,
    FooterTemplateDirective,
    TakePictureComponent
  ]
})
export class CoreModule { }
