import { Directive } from "@angular/core";

@Directive({ selector: '[gridHeader]'}) export class HeaderTemplateDirective {}
@Directive({ selector: '[gridContent]'}) export class ContentTemplateDirective {}
@Directive({ selector: '[gridFooter]'}) export class FooterTemplateDirective {}