import { Directive, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmComponent } from '../components/confirm/confirm.component';

@Directive({
  selector: '[appConfirm]'
})
export class ConfirmDirective {

  @Input() public title?: string;
  @Input() public question?: string;
  @Output() public confirmMethod: EventEmitter<void> = new EventEmitter<void>();

  // @HostBinding('style') c = 'color:red;';

  @HostListener('click')
  elementClicked() {
    this.openModal();
  }

  constructor(private modalService: NgbModal) { }

  private openModal(): void {
    const modalRef: NgbModalRef = this.modalService.open(ConfirmComponent);

    const instance: ConfirmComponent = modalRef.componentInstance;
    instance.title = this.title;
    instance.question = this.question;
    instance.yesClicked = () => {
      this.confirmMethod.emit();
      modalRef.close();
    };
    instance.noClicked = () => modalRef.close();

  }
}
