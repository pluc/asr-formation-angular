import { inject, TestBed } from '@angular/core/testing';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDirective } from './confirm.directive';

describe('ConfirmDirective', () => {

  TestBed.configureTestingModule({
    providers: [NgbModal]
  })

  it('should create an instance', () => {
    const modal: NgbModal = TestBed.inject(NgbModal);

    const directive = new ConfirmDirective(modal);
    expect(directive).toBeTruthy();
  });
});
