import { AbstractControl, AsyncValidatorFn, ValidationErrors } from "@angular/forms";
import { catchError, first, map, Observable, of } from "rxjs";

export function uniqueValidator(values$: Observable<string[]>): AsyncValidatorFn {

    return (control: AbstractControl): Observable<ValidationErrors | null> => {

        return values$.pipe(
            map((values: string[]) => {
                if (values.find(v => v == control.value)) {
                    return { 'unique': true };
                }

                return null;
            }),
            first(),
            catchError(e => {
                console.error(e);
                return of(null);
            })
        );
    }

}