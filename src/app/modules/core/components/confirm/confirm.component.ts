import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  @Input() public title?: string;

  @Input() public question?: string;

  @Input() public yesClicked?: () => void;

  @Input() public noClicked?: () => void;

  @Input() public yesMessage = 'Oui';

  @Input() public noMessage = 'Non';

  constructor() { }

  ngOnInit(): void {
  }

}
