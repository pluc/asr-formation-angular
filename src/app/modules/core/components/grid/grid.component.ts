import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';
import { FooterTemplateDirective, HeaderTemplateDirective } from '../../directives/grid.directives';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {

  @ContentChild(HeaderTemplateDirective, { read: TemplateRef }) headerTemplate?: TemplateRef<any>;
  @ContentChild(FooterTemplateDirective, { read: TemplateRef }) footerTemplate?: TemplateRef<any>;

  constructor() { }

  ngOnInit(): void {
  }

  public helloFromGrid() : void {
    alert('Hello!');
  }
}
