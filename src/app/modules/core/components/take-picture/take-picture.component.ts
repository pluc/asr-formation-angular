import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-take-picture',
  templateUrl: './take-picture.component.html',
  styleUrls: ['./take-picture.component.scss']
})
export class TakePictureComponent implements OnInit {

  public imgUrl?: string;

  ngOnInit(): void {

  }

  uploadFiles(target: any): void {
    const files: FileList = target.files;

    const file = files.item(0);

    const reader = new FileReader();

    reader.onload = e => {
      this.imgUrl = e.target?.result as any;
    };

    reader.readAsDataURL(file as any);
  }

}
