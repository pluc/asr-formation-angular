import { Component, Input, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { AppService } from './app.service';
import { FranchiseService } from './services/franchise.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'TPSaisieDesTemps';

  public pageTitle$?: Observable<string>;

  public routes: { label: string, url: string }[] = [];

  constructor(private appService: AppService, private franchiseService: FranchiseService) {
  }

  ngOnInit(): void {
    this.routes = [
      { label: 'Ressources', url: '/resources' },
      { label: 'Template démo', url:'/tempdemo'}
    ];

    this.pageTitle$ = this.appService.title$;

    console.log(this.franchiseService.GetEntities());
  }
}
