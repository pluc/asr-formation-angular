import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private titleChanged$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public title$: Observable<string> = this.titleChanged$.asObservable();

  public notifyPageTitle(title: string): void {
    console.log('notifying new page title : '+ title);
    this.titleChanged$.next(title);
  }
}
