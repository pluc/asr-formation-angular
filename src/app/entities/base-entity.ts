export class BaseEntity {
    constructor(public id?: number) { }
}

export class BaseEntityWithLabel extends BaseEntity {
    constructor(
        public label: string,
        public code: string,
        id?: number
    ) {
        super(id);
    }
}