import { BaseEntity } from "./base-entity";
import { Cost } from "./cost";

export class TimeEntry extends BaseEntity {
    constructor(
        id: number,
        public quantity: number,
        public cost: Cost,
        public period: EntryType,
        public date: Date
    ) {
        super(id);
    }
}

export type EntryType = 'hour' | 'day';