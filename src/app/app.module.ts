import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FranchiseAdapter, FranchiseAdapterInjectionToken } from './adapters/franchise.adapter';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GridDemoComponent } from './grid-demo/grid-demo.component';
import { CoreModule } from './modules/core/core.module';


@NgModule({
  declarations: [
    AppComponent,
    GridDemoComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CoreModule
  ],
  providers: [
    { provide: FranchiseAdapterInjectionToken, useClass: FranchiseAdapter }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
