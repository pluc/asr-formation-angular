import { Injectable, Injector } from "@angular/core";
import { FranchiseAdapterInjectionToken } from "../adapters/franchise.adapter";
import { Franchise } from "../entities/franchise";
import { EntityService } from "./entity.service";

@Injectable({ providedIn: 'root' })
export class FranchiseService extends EntityService<Franchise> {

    constructor(injector: Injector) {
        super(injector, FranchiseAdapterInjectionToken);
    }

}