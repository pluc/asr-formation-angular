import { Inject, Injectable, Injector, Optional, ProviderToken } from "@angular/core";
import { Adapter } from "../adapters/adapter";

@Injectable({ providedIn: 'root' })
export class EntityService<T> {

    private adapter!: Adapter<T>;

    constructor(
        private injector: Injector,
        @Optional() @Inject(null) private tokenAdapter?: ProviderToken<Adapter<T>>) {

        if (this.tokenAdapter != undefined) {
            this.adapter = this.injector.get<Adapter<T>>(this.tokenAdapter);
        }
    }

    public GetEntities(): T[] {
        const mocks = [
            { l: 'titi' },
            { l: 'tata' }
        ];

        if (this.adapter != undefined) {
            return mocks.map(m => this.adapter.adapt(m));
        }

        return mocks as any as T[];

    }
}