import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { GridDemoComponent } from './grid-demo/grid-demo.component';

const routes: Routes = [
  { path: 'projects', component: AppComponent },
  {
    path: 'resources',
    loadChildren: () => import('./modules/resources/resources.module').then(m => m.ResourcesModule)
  },
  {
    path:'tempdemo', 
    component: GridDemoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
